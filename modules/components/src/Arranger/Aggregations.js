import React from 'react';
import { sortBy } from 'lodash';

import { AggsState, AggsQuery } from '../Aggs';
import aggComponents from '../Aggs/aggComponentsMap.js';

export { AggsWrapper } from '../Aggs';

const BaseWrapper = ({ className, ...props }) => (
  <div {...props} className={`aggregations ${className}`} />
);

export const AggregationsListDisplay = ({
  data,
  onValueChange = () => {},
  aggs,
  graphqlField,
  setSQON,
  sqon,
  containerRef,
  componentProps = {
    getTermAggProps: () => ({}),
    getRangeAggProps: () => ({}),
    getBooleanAggProps: () => ({}),
    getDatesAggProps: () => ({}),
  },
  getCustomItems = ({ aggs }) => [], // Array<{index: number, component: Component | Function}>
  customFacets = [],
}) => {
  const aggComponentInstances =
    data &&
    aggs
      .map((agg) => ({
        ...agg,
        ...data[graphqlField].aggregations[agg.field],
        ...data[graphqlField].extended.find((x) => x.field.replace(/\./g, '__') === agg.field),
        onValueChange: ({ sqon, value }) => {
          onValueChange(value);
          setSQON(sqon);
        },
        key: agg.field,
        sqon,
        containerRef,
      }))
      .map((agg) => {
        const customContent =
          customFacets.find((x) => x.content.field === agg.field)?.content || {};

        // console.log('customContent', customContent);

        return {
          ...agg,
          ...customContent,
        };
      })
      .map((agg) => aggComponents[agg.type]?.({ ...agg, ...componentProps }));

  console.log('aggComponentInstances', aggComponentInstances);
  console.log('aggs', aggs);
  console.log('data', data);

  if (aggComponentInstances) {
    // sort the list by the index specified for each component to prevent order bumping
    const componentListToInsert = sortBy(getCustomItems({ aggs }), 'index');
    console.log('componentListToInsert', componentListToInsert);
    // go through the list of inserts and inject them by splitting and joining
    const inserted = componentListToInsert.reduce((acc, { index, component }) => {
      const firstChunk = acc.slice(0, index);
      const secondChunk = acc.slice(index, acc.length);
      return [...firstChunk, component(), ...secondChunk];
    }, aggComponentInstances);
    return inserted;
  } else {
    return aggComponentInstances;
  }
};

const fetchSampleBySqon = async (sqon = null) => {
  const query = `query($sqon: JSON){
    test{
      aggregations(filters: $sqon, include_missing: false, aggregations_filter_themselves: true){
        Analysis__BiologicalSamples__BiologicalSample__BiologicalSample_ExternalAccession{
         bucket_count
       }
     }
    }
  }`;
  const raw = await fetch('https://share4kids.lyon.unicancer.fr/nodejs/file7/graphql', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify({
      query,
      variables: { sqon },
    }),
  });

  const json = await raw.json();
  if (json) {
    console.log(
      'data json of fetchSampleBySqon',
      json?.data?.test?.aggregations
        ?.Analysis__BiologicalSamples__BiologicalSample__BiologicalSample_ExternalAccession
        ?.bucket_count,
    );
  }
};

export const AggregationsList = ({
  onValueChange = () => {},
  setSQON,
  sqon,
  projectId,
  graphqlField,
  style,
  api,
  Wrapper = BaseWrapper,
  containerRef,
  componentProps = {
    getTermAggProps: () => ({}),
    getRangeAggProps: () => ({}),
    getBooleanAggProps: () => ({}),
    getDatesAggProps: () => ({}),
  },
  aggs = [],
  debounceTime,
  getCustomItems,
  customFacets = [],
}) => (
  <AggsQuery
    api={api}
    debounceTime={300}
    projectId={projectId}
    index={graphqlField}
    sqon={sqon}
    aggs={aggs}
    render={({ data }) => {
      // console.log('projectId oh la la', projectId);
      // console.log('aggs ohla la', aggs);

      // if (projectId === 'datasets4_9') {
      //   console.log('alo tao la datasets4_9');
      //   console.log('data oh la la', data);
      // }

      // // -----code test bug BRAIN------
      // // ----------------------------

      // if (sqon) {
      //   console.log('sqon exist', sqon);
      //   fetchSampleBySqon(sqon);
      // } else {
      //   console.log('sqon nottttt exist');
      //   fetchSampleBySqon(null);
      // }
      // if (data) {
      //   const tmp = data[graphqlField].aggregations[
      //     'Dataset_patients__Dataset_patient__TumorPathologyEvents__TumorPathologyEvent__TumorPathologyEvent_MorphologyCode__level_0__LabelValueMeaning'
      //   ]?.buckets.filter((value) => value.key !== 'Medulloblastoma, NOS');
      //   console.log('tmp', tmp);
      //   if (tmp) {
      //     data[graphqlField].aggregations[
      //       'Dataset_patients__Dataset_patient__TumorPathologyEvents__TumorPathologyEvent__TumorPathologyEvent_MorphologyCode__level_0__LabelValueMeaning'
      //     ].buckets = tmp;
      //     console.log('data after', data);
      //   }
      //   // if (data[graphqlField] !== undefined) {
      //   //   data = data[graphqlField].aggregations[
      //   //     'Dataset_patients__Dataset_patient__Patient_Gender__LabelValueMeaning'
      //   //   ].buckets.filter((value) => value.key !== 'Unknown');
      //   // }

      //   // if (listGender.length) {
      //   //   console.log('Co list gender', listGender);
      //   // }
      // }

      // -----------end code bug BRAIN -------------
      // ----------------------------------------

      return AggregationsListDisplay({
        data,
        onValueChange,
        aggs,
        graphqlField,
        setSQON,
        sqon,
        containerRef,
        componentProps,
        getCustomItems,
        customFacets,
      });
    }}
  />
);

/**
 * customFacets allows custom content to be passed to each facet in the aggregation list.
 *   This can overwrite any property in the agg object in the aggregation list
 *   The structure of this property is:
 *   [
 *     {
 *       content: {
 *         field: 'field_name', // identify which facet this object customizes
 *         displayName: 'New Display Name for This Field', // modify displayName of the facet
 *       },
 *     },
 *   ]
 *
 */
const Aggregations = ({
  onValueChange = () => {},
  setSQON,
  sqon,
  projectId,
  graphqlField,
  className = '',
  style,
  api,
  Wrapper = BaseWrapper,
  containerRef,
  componentProps = {
    getTermAggProps: () => ({}),
    getRangeAggProps: () => ({}),
    getBooleanAggProps: () => ({}),
    getDatesAggProps: () => ({}),
  },
  customFacets = [],
}) => {
  return (
    <Wrapper style={style} className={className}>
      <AggsState
        api={api}
        projectId={projectId}
        graphqlField={graphqlField}
        render={(aggsState) => {
          // console.log("aggsState",aggsState);
          // console.log("api",api);
          const aggs = aggsState.aggs.filter((x) => x.show);
          // console.log("aggs",aggs);

          return (
            <AggregationsList
              onValueChange={onValueChange}
              setSQON={setSQON}
              style={style}
              Wrapper={Wrapper}
              containerRef={containerRef}
              componentProps={componentProps}
              api={api}
              debounceTime={300}
              projectId={projectId}
              graphqlField={graphqlField}
              sqon={sqon}
              aggs={aggs}
              customFacets={customFacets}
            />
          );
        }}
      />
    </Wrapper>
  );
};

export default Aggregations;

import React, { Fragment, useState } from 'react';
import _ from 'lodash';

import { CaretUpOutlined, CaretDownOutlined } from '@ant-design/icons';

import './CustomCellList.css';

const CustomCellList = ({ values, type }) => {
  let listUnique = values ? [...new Set(values)] : [];
  // console.log("listUnique", listUnique);
  listUnique = _.remove(listUnique, function (n) {
    return n !== null;
  });
  const nbListUnique = listUnique?.length;
  // console.log(`${type}`, nbListUnique);
  const [collapse, setCollapse] = useState(false);
  const collapseDown =
    nbListUnique != 0 ? (
      <div onClick={() => setCollapse(!collapse)} className="collapse">
        <CaretDownOutlined />
        <span>
          {nbListUnique} {type}
        </span>
      </div>
    ) : null;
  const collapseUp = (
    <div onClick={() => setCollapse(!collapse)} className="collapse collapseUp">
      <CaretUpOutlined />
      <span>collapse</span>
    </div>
  );
  const ListItems = (
    <ul className="ulListItemsCell">
      {listUnique.map((value, index) => (
        <li key={index}>{value}</li>
      ))}
    </ul>
  );

  const ListOneItem = (
    <ul className="ulListOneItemCell">
      {listUnique.map((value, index) => (
        <li key={index}>{value}</li>
      ))}
    </ul>
  );

  return (
    // <Fragment>
    //   {collapse ? (
    //     <div>
    //       {ListItems}
    //       {collapseUp}
    //     </div>
    //   ) : (
    //     collapseDown
    //   )}
    // </Fragment>
    <Fragment>
      {nbListUnique > 1 ? (
        <>
          {collapse ? (
            <div>
              {ListItems}
              {collapseUp}
            </div>
          ) : (
            collapseDown
          )}
        </>
      ) : (
        ListOneItem
      )}
    </Fragment>
  );
};

export default CustomCellList;
